<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetallePedidoIngrediente extends Model
{
    protected $table = 'detalles_pedido_ingredientes';
}
