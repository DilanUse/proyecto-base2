<?php

use App\Cliente;
use App\DetallePedido;
use App\DetallePedidoIngrediente;
use App\Ingrediente;
use App\Pedido;
use App\Producto;
use App\TipoProducto;
use App\Sucursal;
use Illuminate\Database\Seeder;

class VentasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fecha = date("2019-07-01");

        for (; $fecha != date('Y-m-d'); $fecha = date("Y-m-d",strtotime("$fecha + 1 days"))) {
            for ($pedidosPorDia = random_int(5, 10); $pedidosPorDia > 0; $pedidosPorDia--) {

                $turno = random_int(1, 10);
                $hora = 0;
                $minuto = random_int(0, 59);
                $segundo = random_int(0, 59);

                // mañana
                if ($turno < 2) {
                    $hora = random_int(9, 11);
                }
                elseif ($turno < 4) { // tarde
                    $hora = random_int(12, 18);
                }
                else { // noche
                    $hora = random_int(19, 23);

                }

                $hora = $hora < 10 ? "0$hora": $hora;
                $minuto = $minuto < 10 ? "0$minuto" : $minuto;
                $segundo = $segundo < 10 ? "0$segundo" : $segundo;

                $pedido = Pedido::create([
                    'sucursal_id' => random_int(1, Sucursal::count()),
                    'cliente_cedula' =>  Cliente::inRandomOrder()->first()->cedula,
                    'fecha' => "$fecha $hora:$minuto:$segundo",
                    'monto' => 0,
                    'utilidad' => 0,
                ]);

                $monto = 0;
                $costo = 0;


                for($detalles = random_int(1, 15); $detalles > 0; $detalles--) {

                    $tipoProducto = random_int(1, 2);

                    $item = Producto::where('tipo_producto_id', '=', $tipoProducto)
                        ->inRandomOrder()->first();

                    $detalle = DetallePedido::create([
                        'pedido_id' => $pedido->id,
                        'producto_id' =>  $item->id,
                        'precio' => $item->precio,
                        'costo' => $item->costo,
                    ]);


                    $monto += $item->precio;
                    $costo += $item->costo;

                    if($item->tipo_producto_id == 1) {
                        for($ingredientes = random_int(0, Ingrediente::count()); $ingredientes > 0; $ingredientes--) {
                            DetallePedidoIngrediente::create([
                                'detalle_pedido_id' => $detalle->id,
                                'ingrediente_id' =>  $ingredientes,
                            ]);
                        }
                    }
                }

                $pedido->monto = $monto;
                $pedido->utilidad = $monto - $costo;
                $pedido->save();

            }
        }
    }
}
