<?php

use App\Ingrediente;
use Illuminate\Database\Seeder;

class IngredientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ingrediente::create([
            'nombre' => 'Zanahoria'
        ]);

        Ingrediente::create([
            'nombre' => 'Cebolla'
        ]);

        Ingrediente::create([
            'nombre' => 'Papas'
        ]);

        Ingrediente::create([
            'nombre' => 'Queso'
        ]);

        Ingrediente::create([
            'nombre' => 'Salsas'
        ]);
    }
}
