<?php

use App\TipoProducto;
use Illuminate\Database\Seeder;

class TiposProductosTableSeeder extends Seeder
{
    /**
     * Run the database sTipoProductoeeds.
     *
     * @return void
     */
    public function run()
    {
        TipoProducto::create([
            'nombre' => 'Perro Caliente'
        ]);

        TipoProducto::create([
            'nombre' => 'Refresco'
        ]);

        TipoProducto::create([
            'nombre' => 'Jugo Natural'
        ]);

        TipoProducto::create([
            'nombre' => 'Nestea'
        ]);

        TipoProducto::create([
            'nombre' => 'Agua'
        ]);
    }
}
