<?php

use App\Producto;
use Illuminate\Database\Seeder;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Producto::create([
            'tipo_producto_id' => 1,
            'nombre' => 'Frankfurt',
            'descripcion' => 'Es una salchicha elaborada mediante cocción en agua caliente o en horno, hecha de pura carne de cerdo, su sabor especial se logra por el proceso de ahumado.',
            'precio' => 15000.00,
            'costo' => 10000.00,
        ]);

        Producto::create([
            'tipo_producto_id' => 1,
            'nombre' => 'Viena',
            'descripcion' => 'Es una variante de la salchicha tipo Frankfurt que a diferencia de esta, se hace con carne de ternera y de cerdo.',
            'precio' => 10000.00,
            'costo' => 5000.00,
        ]);

        Producto::create([
            'tipo_producto_id' => 1,
            'nombre' => 'Polaca',
            'descripcion' => 'Igual que las salchichas tipo Alemana, existen diferentes tipos de salchichas Polacas, elaboradas con cerdo, pavo o ternera.',
            'precio' => 20000.00,
            'costo' => 12000.00,
        ]);

        Producto::create([
            'tipo_producto_id' => 1,
            'nombre' => 'Parrillera',
            'descripcion' => 'Son especiales para parrillas, están elaboradas con carne de res, cerdo, grasa, especias y condimentos.',
            'precio' => 15000.00,
            'costo' => 10000.00,
        ]);

        Producto::create([
            'tipo_producto_id' => 1,
            'nombre' => 'Chorizo',
            'descripcion' => 'Es un embutido originario y típico de la Península Ibérica que se ha extendido a toda Latinoamérica. Hay infinidades de tipos y clases de chorizos, elaborados con todo tipo de carnes y diferentes tipos de condimentos y especias, como el chorizo español, criollo, con ajo, ahumado, picante, de pollo, de pavo, etc.',
            'precio' => 18000.00,
            'costo' => 11000.00,
        ]);

        Producto::create([
            'tipo_producto_id' => 2,
            'nombre' => 'Coca-Cola',
            'descripcion' => '',
            'precio' => 3000.00,
            'costo' => 1500.00,
        ]);

        Producto::create([
            'tipo_producto_id' => 2,
            'nombre' => 'Frescolita',
            'descripcion' => '',
            'precio' => 3000.00,
            'costo' => 1500.00,
        ]);

        Producto::create([
            'tipo_producto_id' => 2,
            'nombre' => 'Postobón',
            'descripcion' => '',
            'precio' => 4000.00,
            'costo' => 2000.00,
        ]);
    }
}
