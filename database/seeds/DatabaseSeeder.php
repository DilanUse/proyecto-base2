<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ClientesTableSeeder::class,
            SucursalesTableSeeder::class,
            TiposProductosTableSeeder::class,
            ProductosTableSeeder::class,
            IngredientesTableSeeder::class,
            VentasSeeder::class
        ]);
    }
}
