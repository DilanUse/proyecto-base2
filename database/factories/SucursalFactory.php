<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Sucursal::class, function (Faker $faker) {
    return [
        'nombre' => $faker->unique()->company,
    ];
});
