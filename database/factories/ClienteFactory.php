<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(\App\Cliente::class, function (Faker $faker) {
    return [
        'cedula' => $faker->unique()->numberBetween(),
        'nombres' => $faker->firstName() . ' ' . $faker->lastName,
        'telefono' => $faker->phoneNumber,
        'direccion' => $faker->address,
        'genero' => random_int(0, 1) == 0 ? 'M' : 'F',
        'edad' => random_int(16, 80),
    ];
});
