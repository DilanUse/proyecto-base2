<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sucursal_id');
            $table->unsignedBigInteger('cliente_cedula');
            $table->dateTime('fecha');
            $table->decimal('monto');
            $table->decimal('utilidad');
            $table->timestamps();

            $table->foreign('sucursal_id')
                ->references('id')->on('sucursales')
                ->onDelete('cascade');

            $table->foreign('cliente_cedula')
                ->references('cedula')->on('clientes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->dropForeign(['sucursal_id', 'cliente_cedula']);
        });

        Schema::dropIfExists('pedidos');
    }
}
