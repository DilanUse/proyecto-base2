<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('tipo_producto_id');
            $table->string('nombre', 25);
            $table->string('descripcion', 500);
            $table->decimal('precio');
            $table->decimal('costo');
            $table->timestamps();

            $table->foreign('tipo_producto_id')
                ->references('id')->on('tipos_productos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productos', function (Blueprint $table) {
            $table->dropForeign(['tipo_producto_id']);
        });

        Schema::dropIfExists('productos');
    }
}
