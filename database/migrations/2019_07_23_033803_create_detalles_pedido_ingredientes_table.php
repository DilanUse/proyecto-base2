<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallesPedidoIngredientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles_pedido_ingredientes', function (Blueprint $table) {
            $table->unsignedBigInteger('detalle_pedido_id');
            $table->unsignedBigInteger('ingrediente_id');
            $table->timestamps();

            $table->primary(['detalle_pedido_id', 'ingrediente_id'], 'dp_ingredientes_primary');

         

            $table->foreign('ingrediente_id')
                ->references('id')->on('ingredientes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalles_pedido_ingredientes', function (Blueprint $table) {
            $table->dropForeign(['detalle_pedido_id', 'ingrediente_id']);
        });

        Schema::dropIfExists('detalles_pedido_ingredientes');
    }
}
