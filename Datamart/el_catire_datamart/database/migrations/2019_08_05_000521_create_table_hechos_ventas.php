<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHechosVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Hechos_Ventas', function (Blueprint $table) {
            

            $table->integer('cantidad_Perros');

            //-------------------------------CAMPOS REFERENCIAS DE OTRAS TABLAS-------------------------
            
             $table->unsignedBigInteger('dimension_sucursal_id');
             $table->unsignedBigInteger('dimension_turno_id');
             $table->unsignedBigInteger('dimension_tipos_perros_id');
             $table->date('dimension_tiempo_fecha_completa');

             $table->primary(['dimension_sucursal_id', 'dimension_turno_id','dimension_tipos_perros_id','dimension_tiempo_fecha_completa'], 'dp_hechos-ventas_primary');

              $table->foreign('dimension_sucursal_id')
                ->references('id')->on('Dimension_Sucursal')
                ->onDelete('cascade');

                 $table->foreign('dimension_turno_id')
                ->references('id')->on('Dimension_Turno')
                ->onDelete('cascade');

                  $table->foreign('dimension_tipos_perros_id')
                ->references('id')->on('Dimension_Tipos_Perros')
                ->onDelete('cascade');

                $table->foreign('dimension_tiempo_fecha_completa')
                ->references('fecha_Completa')->on('Dimension_Tiempo')
                ->onDelete('cascade');



            //------------------------------------------------
            


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('Hechos_Ventas', function (Blueprint $table) {
            $table->dropForeign(['dimension_sucursal_id', 'dimension_turno_id','dimension_tipos_perros_id','dimension_tiempo_fecha_completa']);
        });

        Schema::dropIfExists('Hechos_Ventas');
    }
}
