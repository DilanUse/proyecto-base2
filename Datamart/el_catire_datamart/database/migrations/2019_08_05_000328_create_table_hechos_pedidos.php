<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHechosPedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Hechos_Pedidos', function (Blueprint $table) {
       
            $table->float('porcent_S1I',5,2);
            $table->float('porcent_S2I',5,2);
            $table->float('porcent_ConTodo',5,2);
            $table->float('porcent_SinIngre',5,2);
            $table->float('porcent_ConRef',5,2);
            $table->integer('cantidad_Clientes');
            $table->double('ganancias',10,2);



            //Referencias de otras tablas

                $table->unsignedBigInteger('dimension_sucursal_id');
                $table->date('dimension_tiempo_fecha_completa');

                $table->primary(['dimension_sucursal_id', 'dimension_tiempo_fecha_completa'], 'dp_hechos-pedidos_primary');

                $table->foreign('dimension_sucursal_id')
                ->references('id')->on('Dimension_Sucursal')
                ->onDelete('cascade');

                $table->foreign('dimension_tiempo_fecha_completa')
                ->references('fecha_Completa')->on('Dimension_Tiempo')
                ->onDelete('cascade');

                

            //----------------------------

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {


        Schema::table('Hechos_Pedidos', function (Blueprint $table) {
            $table->dropForeign(['dimension_sucursal_id', 'dimension_tiempo_fecha_completa']);
        });


        Schema::dropIfExists('Hechos_Pedidos');
    }
}
