<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDimensionTiempo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Dimension_Tiempo', function (Blueprint $table) {
            
            $table->date('fecha_Completa')->primary();
            $table->integer('dia');
            $table->integer('mes');
            $table->year('año');
            $table->enum('dia_sem', ['DOMINGO', 'LUNES','MARTES','MIERCOLES','JUEVES','VIERNES','SABADO']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Dimension_Tiempo');
    }
}
